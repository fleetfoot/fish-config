source ~/.config/fish/.aliases

starship init fish | source

set -gx EDITOR emacs

status --is-interactive; and rbenv init - fish | source
zoxide init fish | source
